<?php

namespace Drupal\commerce_makesyoulocal\Event;

use Drupal\commerce\EventBase;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the order request event.
 *
 * @see \Drupal\commerce_makesyoulocal\Event\MakesYouLocalEvents
 */
class OrderRequestEvent extends EventBase {

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The API request data.
   *
   * @var array
   */
  protected $requestData;

  /**
   * Constructs a new OrderRequestEvent object.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $request_data
   *   The API request data.
   */
  public function __construct(OrderInterface $order, array $request_data) {
    $this->order = $order;
    $this->requestData = $request_data;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder() : OrderInterface {
    return $this->order;
  }

  /**
   * Gets the API request data.
   *
   * @return array
   *   The API request data.
   */
  public function getRequestData() {
    return $this->requestData;
  }

  /**
   * Sets the API request data.
   *
   * @param array $request_data
   *   The API request data.
   *
   * @return $this
   */
  public function setRequestData(array $request_data) {
    $this->requestData = $request_data;
    return $this;
  }

}
