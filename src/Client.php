<?php

namespace Drupal\commerce_makesyoulocal;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;

class Client {

  use LoggerChannelTrait;

  public const MAKES_YOU_LOCAL_ORDERS_ENDPOINT = 'https://client.makesyoulocal.com/api/v1/custom/orders';
  public const MAKES_YOU_LOCAL_PRODUCTS_ENDPOINT = 'https://client.makesyoulocal.com/api/v1/custom/products';

  /**
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Client constructor.
   *
   * @param \GuzzleHttp\Client $client
   *   The client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(\GuzzleHttp\Client $client, ConfigFactoryInterface $config_factory) {
    $this->client = $client;
    $this->configFactory = $config_factory;
    $this->settings = $config_factory->get('commerce_makesyoulocal.settings');
  }

  /**
   * Sent data to MakesYouLocal.
   *
   * @param array $payload
   *   The payload to be sent.
   */
  public function send(array $payload) {
    if (isset($payload['orders'])) {
      $endpoint = self::MAKES_YOU_LOCAL_ORDERS_ENDPOINT;
    } else  {
      $endpoint = self::MAKES_YOU_LOCAL_PRODUCTS_ENDPOINT;
    }

    $apiToken = base64_encode($this->settings->get('token'));

    try {
      $request = $this->client->post($endpoint, [
        'json' => $payload,
        'headers' => [
          'Content-Type' => 'application/json',
          'Authorization' => 'Basic ' . $apiToken,
        ],
      ]);
      $response = Json::decode($request->getBody());
    }
    catch (\Exception $exception) {
      $this->getLogger('commerce_makesyoulocal')->error($exception);
    }

    return isset($response['result']['stats']);
  }

}
