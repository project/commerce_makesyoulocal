<?php

namespace Drupal\commerce_makesyoulocal\Event;

/**
 * Defines events for the MakesYouLocal module.
 */
final class MakesYouLocalEvents {

  /**
   * Name of the event fired when sending an order.
   *
   * @Event
   *
   * @see \Drupal\commerce_makesyoulocal\Event\OrderRequestEvent
   */
  const ORDER_REQUEST = 'commerce_makesyoulocal.order_request';

  /**
   * Name of the event fired when sending an product.
   *
   * @Event
   *
   * @see \Drupal\commerce_makesyoulocal\Event\ProductRequestEvent
   */
  const PRODUCT_VARIATION_REQUEST = 'commerce_makesyoulocal.product_variation_request';

}
