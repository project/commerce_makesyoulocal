<?php

namespace Drupal\commerce_makesyoulocal\Plugin\QueueWorker;

use Drupal\commerce_makesyoulocal\Client;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * MakesYouLocal queue worker.
 *
 * @QueueWorker(
 *   id = "makesyoulocal_sync",
 *   title = @Translation("MakesYouLocal Queue"),
 *   cron = {"time" = 60}
 * )
 */
class MakesYouLocalQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The MakesYouLocal client.
   *
   * @var \Drupal\commerce_makesyoulocal\Client
   */
  protected $client;

  /**
   * Constructs a new MakesYouLocalQueue object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_makesyoulocal\Client $client
   *   The MakesYouLocal client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $client) {
    $this->client = $client;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_makesyoulocal.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($message) {
    $this->client->send($message);
  }

}
