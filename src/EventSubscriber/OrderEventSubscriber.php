<?php

namespace Drupal\commerce_makesyoulocal\EventSubscriber;

use Drupal\commerce_makesyoulocal\MakesYouLocalInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderEventSubscriber implements EventSubscriberInterface {

  /**
   * The MYL service.
   *
   * @var \Drupal\commerce_makesyoulocal\MakesYouLocalInterface
   */
  protected $makesYouLocal;

  /**
   * Constructs a new OrderEvenSubscriber object.
   *
   * @param \Drupal\commerce_makesyoulocal\MakesYouLocalInterface $makes_you_local
   *   The MakesYouLocal client.
   */
  public function __construct(MakesYouLocalInterface $makes_you_local) {
    $this->makesYouLocal = $makes_you_local;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.cancel.post_transition' => ['onStatusChange'],
      'commerce_order.place.post_transition' => ['onStatusChange'],
      'commerce_order.validate.post_transition' => ['onStatusChange'],
      'commerce_order.fulfill.post_transition' => ['onStatusChange'],
    ];
  }

  /**
   * Send order updates to MakesYouLocal.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onStatusChange(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $this->makesYouLocal->syncOrders([$order]);
  }

}
