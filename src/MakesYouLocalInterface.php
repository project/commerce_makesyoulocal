<?php

namespace Drupal\commerce_makesyoulocal;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;

interface MakesYouLocalInterface {

  /**
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface[] $product_variations
   *   The product variations.
   */
  public function syncProductVariations(array $product_variations);

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface[] $orders
   *   The order.
   */
  public function syncOrders(array $orders);

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   */
  public function buildOrderPayload(OrderInterface $order);

  /**
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation
   *
   * @return array
   */
  public function buildProductVariationPayload(ProductVariationInterface $product_variation);

}
