<?php

namespace Drupal\commerce_makesyoulocal\Event;

use Drupal\commerce\EventBase;
use Drupal\commerce_product\Entity\ProductVariationInterface;

/**
 * Defines the product variation request event.
 *
 * @see \Drupal\commerce_makesyoulocal\Event\MakesYouLocalEvents
 */
class ProductVariationRequestEvent extends EventBase {

  /**
   * The product.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $productVariation;

  /**
   * The API request data.
   *
   * @var array
   */
  protected $requestData;

  /**
   * Constructs a new ProductVariationRequestEvent object.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation
   *   The product variation.
   * @param array $request_data
   *   The API request data.
   */
  public function __construct(ProductVariationInterface $product_variation, array $request_data) {
    $this->productVariation = $product_variation;
    $this->requestData = $request_data;
  }

  /**
   * Gets the product.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface
   *   The product.
   */
  public function getProductVariation() : ProductVariationInterface {
    return $this->productVariation;
  }

  /**
   * Gets the API request data.
   *
   * @return array
   *   The API request data.
   */
  public function getRequestData() {
    return $this->requestData;
  }

  /**
   * Sets the API request data.
   *
   * @param array $request_data
   *   The API request data.
   *
   * @return $this
   */
  public function setRequestData(array $request_data) {
    $this->requestData = $request_data;
    return $this;
  }

}
