<?php

namespace Drupal\commerce_makesyoulocal_aq\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;
use Drupal\commerce_makesyoulocal\Client;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sync products and order updates to MV.
 *
 * @AdvancedQueueJobType(
 *   id = "makesyoulocal_sync",
 *   label = @Translation("MakesYouLocal Queue"),
 *   max_retries = 3,
 *   retry_delay = 180,
 * )
 */
class MakesYouLocalQueue extends JobTypeBase implements ContainerFactoryPluginInterface {

  /**
   * The MakesYouLocal client.
   *
   * @var \Drupal\commerce_makesyoulocal\Client
   */
  protected $client;

  /**
   * Constructs a new MiddlewareUpdateProduct object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_makesyoulocal\Client $client
   *   The MakesYouLocal client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_makesyoulocal.client')
    );
  }

  public function process(Job $job) {
    $payload = $job->getPayload();

    // We need to have at-least one order or sku.
    $order_id = $payload['orders'][0]['order_number'] ?? NULL;
    $product_sku = $payload['products'][0]['sku'] ?? NULL;

    if (empty($order_id) && empty($product_sku)) {
      return JobResult::failure('Malformed queue entry for MakesYouLocal');
    }

    $sent = $order_id ? 'Orders' : 'Products';

    if ($this->client->send($payload)) {
      return JobResult::success(sprintf('%s sync success', $sent));
    }

    return JobResult::failure(sprintf('%s sync failed', $sent));

  }

}
