<?php

namespace Drupal\commerce_makesyoulocal;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\address\AddressInterface;
use Drupal\advancedqueue\Job;
use Drupal\commerce_makesyoulocal\Event\MakesYouLocalEvents;
use Drupal\commerce_makesyoulocal\Event\OrderRequestEvent;
use Drupal\commerce_makesyoulocal\Event\ProductVariationRequestEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\file\FileInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The MakesYouLocal sync service.
 */
class MakesYouLocal implements MakesYouLocalInterface {

  /**
   * The MakesYouLocal client.
   *
   * @var \Drupal\commerce_makesyoulocal\Client
   */
  protected $client;

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The commerce_makesyoulocal settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The country list.
   *
   * @var array
   */
  protected $countryList;

  /**
   * Constructs the MakesYouLocal object.
   *
   * @param \Drupal\commerce_makesyoulocal\Client $client
   *   The MakesYouLocal client.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue factory service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface $country_repository
   *   The country repository.
   */
  public function __construct(Client $client, QueueFactory $queue, ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, CountryRepositoryInterface $country_repository) {
    $this->client = $client;
    $this->queue = $queue;
    $this->configFactory = $config_factory;
    $this->settings = $config_factory->get('commerce_makesyoulocal.settings');
    $this->eventDispatcher = $event_dispatcher;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->countryList = $country_repository->getList();
  }

  /**
   * {@inheritdoc}
   */
  public function syncOrders(array $orders) {
    $payload = [];
    foreach ($orders as $order) {
      // Skip orders without an order number set or with a NULL total.
      if (empty($order->getOrderNumber()) || empty($order->getTotalPrice())) {
        continue;
      }
      $payload['orders'][] = $this->buildOrderPayload($order);
    }
    if ($payload) {
      $this->sync($payload);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function syncProductVariations(array $product_variations) {
    $payload = [];
    foreach ($product_variations as $product_variation) {
      if (!$product_variation->getProduct()) {
        continue;
      }
      if ($data = $this->buildProductVariationPayload($product_variation)) {
        $payload['products'][] = $data;
      }
    }

    if (!empty($payload)) {
      $this->sync($payload);
    }
  }

  /**
   * @param array $payload
   */
  protected function sync(array $payload) {
    // Support for advancedqueue and core one.
    if (!empty($this->settings->get('queue'))) {
      if ($this->moduleHandler->moduleExists('commerce_makesyoulocal_aq')) {
        $queue_storage = $this->entityTypeManager->getStorage('advancedqueue_queue');
        /** @var \Drupal\advancedqueue\Entity\QueueInterface $queue */
        $queue = $queue_storage->load('commerce_makesyoulocal');

        $capture_job = Job::create('makesyoulocal_sync', $payload);
        $queue->enqueueJob($capture_job);
      }
      else {
        $this->queue->get('makesyoulocal_sync')->createItem($payload);
      }
    }
    else {
      $this->client->send($payload);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrderPayload(OrderInterface $order) {
    $data =  [
      'order_number' => $order->getOrderNumber(),
      'remote_id' => $order->uuid(),
      'created_at' => DrupalDateTime::createFromTimestamp($order->getCreatedTime())->format('c'),
      'status' => $this->mapOrderStates($order->getState()->getId()),
      'email' => $order->getEmail(),
      'price' => $order->getTotalPrice()->getNumber(),
      'currency' => $order->getTotalPrice()->getCurrencyCode(),
      'ip' => $order->getIpAddress(),
    ];

    $profiles = $order->collectProfiles();

    if (isset($profiles['billing']) && $address = $profiles['billing']->address->first()) {
      $data['billing_address'] = $this->mapOrderAddress($address);
    }

    if (isset($profiles['shipping']) && $address = $profiles['shipping']->address->first()) {
      $data['shipping_address'] = $this->mapOrderAddress($address);
    }

    // Add order line items.
    foreach ($order->getItems() as $orderItem) {
      $purchasable_entity = $orderItem->getPurchasedEntity();
      $data['order_lines'][] = [
        'sku' => $purchasable_entity->getSku(),
        'name' => $orderItem->getTitle(),
        'quantity' => $orderItem->getQuantity(),
        'price' => $orderItem->getTotalPrice()->getNumber()
      ];
    }

    // Included order not included adjustment. Treat as separate order lines.
    foreach ($order->getAdjustments() as $adjustment) {
      // Skip shipping, it's processed separately.
      if (!$adjustment->isIncluded() && $adjustment->isPositive() && $adjustment->getType() !== 'shipping') {
        $data['order_lines'][] = [
          'sku' => $adjustment->getType(),
          'name' => $adjustment->getLabel(),
          'quantity' => 1,
          'price' => $adjustment->getAmount()->getNumber()
        ];
      }
    }

    if (!$order->get('payment_gateway')->isEmpty())  {
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      /** @var \Drupal\commerce_payment\Entity\PaymentInterface[] $payments */
      if ($payments = $payment_storage->loadMultipleByOrder($order)) {
        $payment = end($payments);
        /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
        $payment_gateway = $payment->getPaymentGateway();
        $data['payment'] = [
          'id' => $payment_gateway->id(),
          'name' => $payment_gateway->label(),
          'price' => $payment->getAmount()->getNumber()
        ];
      }
    }

    // Handle shipments if there is any.
    if ($order->hasField('shipments')) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
      if ($shipments = $order->get('shipments')->referencedEntities()) {
        $shipment = end($shipments);

        $data['shipping'] = [
          'id' => $shipment->getShippingMethodId(),
          'name' => $shipment->getShippingService(),
          'price'=> $shipment->getAmount()->getNumber()
        ];

        foreach ($shipments as $shipment) {
          if ($shipment->getShippedTime()) {
            $data['shipments'][] = [
              'courier' => $shipment->getShippingService(),
              'tracking' => $shipment->getTrackingCode(),
              'shipped_at' => DrupalDateTime::createFromTimestamp($shipment->getShippedTime())->format('c'),
            ];
          }
        }
      }
    }

    $event = new OrderRequestEvent($order, $data);
    $this->eventDispatcher->dispatch($event, MakesYouLocalEvents::ORDER_REQUEST);

    return $event->getRequestData();
  }

  /**
   * Map address to MakesYouLocal format.
   *
   * @param \Drupal\address\AddressInterface $address
   *   The address data.
   *
   * @return array
   *   Formatted output.
   */
  protected function mapOrderAddress(AddressInterface $address) {
    $data =  [
      "first_name" => $address->getGivenName(),
      "last_name" => $address->getFamilyName(),
      "address_1" => $address->getAddressLine1(),
      "address_2" => $address->getAddressLine2(),
      "zip_code" => $address->getPostalCode(),
      "city" => $address->getLocality(),
      "country" => [
        "id" => $address->getCountryCode(),
        "name" => $this->countryList[$address->getCountryCode()],
      ],
    ];

    if (!empty($address->getName())) {
      $data['name'] = $address->getName();
    }

    if (!empty($address->getAdministrativeArea())) {
      $data['state'] = $address->getAdministrativeArea();
    }

    return $data;
  }

  /**
   * Helper ot map Commerce to MakesYouLocal statuses.
   *
   * @param string $state
   *   Order transition state.
   *
   * @return string
   *   Return mapped state.
   */
  protected function mapOrderStates(string $state) {
    $mapping =  [
      'draft' => 'open',
      'validation' => 'open',
      'pending' => 'open',
      'fulfillment' => 'open',
      'completed' => 'closed',
      'canceled' => 'cancelled',
    ];

    // Provide easy way to map order status.
    $mapping += $this->moduleHandler->invokeAll('commerce_makesyoulocal_mapping_alter', [$mapping]);
    return $mapping[$state] ?? 'open';
  }

  /**
   * {@inheritdoc}
   */
  public function buildProductVariationPayload(ProductVariationInterface $product_variation) {
    $product = $product_variation->getProduct();

    $payload = [
      'sku' => $product_variation->getSku(),
      'name'=> $product->getTitle(),
      'url' => $product->toUrl()->toString(),
    ];

    if ($price = $product_variation->getPrice()) {
      $payload['price'] = $price->getNumber();
      $payload['currency'] = $price->getCurrencyCode();
    }

    if ($product_variation->hasField('stock')) {
      $payload['stock'] = $product_variation->get('stock')->getString();
      $payload['stock_message'] = $payload['stock'] > 0 ? 'In stock' : 'Out of stock';
    }

    /** @var \Drupal\commerce_product\Entity\ProductTypeInterface $product_type */
    $product_type = $this->entityTypeManager->getStorage('commerce_product_type')->load($product->bundle());
    $mapping = $product_type->getThirdPartySetting('commerce_makesyoulocal', 'mapping', []);

    foreach ($mapping['product'] as $id => $drupal_field_id) {
      $payload += $this->getMappedFieldValue($product, $drupal_field_id, $id);
    }

    foreach ($mapping['product_variation'] as $id => $drupal_field_id) {
      $payload += $this->getMappedFieldValue($product_variation, $drupal_field_id, $id);
    }

    $event = new ProductVariationRequestEvent($product_variation, $payload);
    $this->eventDispatcher->dispatch($event, MakesYouLocalEvents::PRODUCT_VARIATION_REQUEST);

    return $event->getRequestData();
  }

  /**
   * Helper for mapped fields.
   */
  protected function getMappedFieldValue($entity, $drupal_field_id, $field_id) {
    $output = [];
    if ($entity->hasField($drupal_field_id) && !$entity->get($drupal_field_id)->isEmpty()) {
      switch ($field_id) {
        case 'offer_price':
        case 'price':
          $price = $entity->get($drupal_field_id)->first()->toPrice();
          $output[$field_id] = $price->getNumber();
          $output['currency'] = $price->getCurrencyCode();
          break;

        case 'categories':
          $references = $entity->get($drupal_field_id)->referencedEntities();
          $terms = [];

          foreach ($references as $reference) {
            $terms[] = [
              'id' => $reference->id(),
              'name' => $reference->label()
            ];
          }

          $output[$field_id] = $terms;
          break;

        case 'image':
          $references = $entity->get($drupal_field_id)->referencedEntities();
          $reference = end($references);
          if ($reference instanceof FileInterface) {
            $output[$field_id] = $reference->createFileUrl(FALSE);
          }
          break;

        default:
          $output[$field_id] = $entity->get($drupal_field_id)->value;
          break;
      }
    }

    return $output;
  }

}
