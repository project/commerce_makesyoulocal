CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Product sync
* Order sync
* Maintainers

INTRODUCTION
------------

Provides integration with [MakesYouLocal](https://www.makesyoulocal.com/) service.

Features
1. Sync products after product / product variation updates
2. Sync orders after orders move through specific transitions
3. Use core queue or AdvancedQueue for syncing with MakesYouLocal

REQUIREMENTS
------------
This module requires Drupal Commerce 2 and it's submodule order and product.


INSTALLATION
------------
Install the Commerce MakesYouLocal module as you would normally install
any Drupal contrib module.
Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
--------------
    1. Navigate to Administration > Extend and enable the Commerce MakesYouLocal
       module.
    2. Navigate to Home > Administration > Commerce > Configuration
                   > Commerce MakesYouLocal.


PRODUCT SYNC
------------
MakesYouLocal provides two ways how products and product variation can be synced.

Drupal Commerce does not have master sku, so this module provides flat syncing
of product and product variations towards MakesYouLocal.

One entry which combine data from product variation and his parent product.

```json
{
   "products":[
      {
         "sku":"1235",
         "name":"Baseball Cap",
         "description_1":"Nice gap",
         "description_2":"Very nice cap",
         "url":"https://example.com/products/1235",
         "price":399.00,
         "offer_price":318.00,
         "currency":"USD",
         "image":"https://example.com/images/1235.png",
         "categories":[
            {
               "id":"5679",
               "name":"Baseball Caps"
            }
         ],
         "stock":1,
         "stock_message":"In stock",
         "ean":"572380129349"
      }
   ]
}
```

This module additionally:
1. Provide mapping inside UI for product / product variation fields inside Product Type edit
   `/admin/commerce/config/product-types`
2. Provides event which can alter payload for product and product variation

Configuration:
Visit `/admin/commerce/config/product-types` and configure each product type separately


ORDER SYNC
------------
Current integration sends update towards MakesYouLocal on several core order transition trough
events. See `\Drupal\commerce_makesyoulocal\EventSubscriber\OrderEventSubscriber`

There is hook which provides option to alter default provided mapping for state between Commerce and
MakesYouLocal

```
function hook_commerce_makesyoulocal_mapping_alter($mapping) {
  $mapping['commerce_specific_status'] = 'open';
}
```

Default mapping is:
```
$mapping =  [
  'draft' => 'open',
  'validation' => 'open',
  'pending' => 'open',
  'fulfillment' => 'open',
  'completed' => 'closed',
  'canceled' => 'cancelled',
];
```

This module provides event which can alter payload for orders.

Configuration:
Visit `/admin/commerce/config/order-types` and configure each order type separately

MAINTAINERS
-----------

Developed and maintained by:
* Centarro - https://www.drupal.org/centarro

Sponsored by:
* Matsmart - https://www.drupal.org/matsmart
