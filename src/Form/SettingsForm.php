<?php

namespace Drupal\commerce_makesyoulocal\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Defines the MakesYouLocal settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_makesyoulocal_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_makesyoulocal.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('commerce_makesyoulocal.settings');

    $form['queue'] = [
      '#type' => 'checkbox',
      '#default_value' => $config->get('queue'),
      '#title' => $this->t('Queue updates'),
      '#description' => $this->t('Enable sending any data to MakesYouLocal trough Queue.'),
    ];

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => 'API Token',
      '#description' => $this->t('MakesYouLocal your API token'),
      '#default_value' => $config->get('token') ?? ''
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('commerce_makesyoulocal.settings');
    $values = $form_state->getValues();
    foreach (['queue', 'token'] as $key) {
      if (!isset($values[$key])) {
        continue;
      }
      $config->set($key, $values[$key]);
    }
    $config->save();
  }

}
